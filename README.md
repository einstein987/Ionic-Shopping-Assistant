IONIC QUICK TUTORIAL

* Setup this project to use Sass: ionic setup sass

* Develop in the browser with live reload: ionic serve

* Add a platform (ios or Android): ionic platform add ios [android]

* Build your app: ionic build <PLATFORM>

* Simulate your app: ionic emulate <PLATFORM>

* Run your app on a device: ionic run <PLATFORM>

* Package an app using Ionic package service: ionic package <MODE> <PLATFORM>

For more help use ionic --help or ionic docs
Visit the Ionic docs: http://ionicframework.com/docs