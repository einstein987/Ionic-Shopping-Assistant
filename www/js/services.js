angular.module('starter.services', [
  'starter.services.list', 
  'starter.services.sublist', 
  'starter.services.product',
  'starter.services.setting'
]);