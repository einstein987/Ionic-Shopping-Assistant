angular.module('translation.pl', ['pascalprecht.translate'])

.config(['$translateProvider', function ($translateProvider) {
  $translateProvider.translations('pl', {
  	'LANG_NAME': 'Polski',

  	// Overall actions
    'OK': 'Zatwierdź',
    'SAVE': 'Zapisz',
  	'CANCEL': 'Anuluj',
  	'SEARCH': 'Szukaj',
  	'ADD': 'Dodaj',
  	'ARCHIVE': 'Archiwizuj',
    'ARCHIVE_MANY': 'Archiwizuj wiele',
    'SELECT_TO_ARCHIVE': 'Zaznacz do archiwizacji',
    'EDIT': 'Edytuj',
  	'DELETE': 'Usuń',
    'DELETE_MANY': 'Usuń wiele',
    'SELECT_TO_DELETE': 'Zaznacz do usunięcia',

    // Tab lists
  	'SHOPPING_LISTS': 'Listy zakupów',
  	'LISTS': 'Listy',
  	'NO_LISTS_INFO': 'Aktualnie nie posiadasz żadnej listy.',
    'ADD_LIST': 'Dodaj listę',
    'TITLE': 'Nazwa',
    'NOTES': 'Notatki',
    'ADD_NOTES': 'Dodaj notatki',
    'DESCRIPTION': 'Opis',

    // Tab products
  	'PRODUCTS': 'Produkty',
    'NO_PRODUCTS_INFO': 'Aktualnie nie posiadasz żadnych produktów.',
    'PRICE': 'Cena',
    'ADD_PRODUCT': 'Dodaj produkt',
    'EDIT_PRODUCT': 'Edytuj produkt',

    // Tab settings
  	'SETTINGS': 'Ustawienia',
  	'LANGUAGE': 'Język',
  	'VERSION': 'Wersja',
  	'CHANGELOG': 'Zmiany',

  });
}]);
