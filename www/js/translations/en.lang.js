angular.module('translation.en', ['pascalprecht.translate'])

.config(['$translateProvider', function ($translateProvider) {
  $translateProvider.translations('en', {
    'LANG_NAME': 'English',

  	// Overall actions
  	'OK': 'OK',
    'SAVE': 'Save',
  	'CANCEL': 'Cancel',
  	'SEARCH': 'Search',
  	'ADD': 'Add',
  	'ARCHIVE': 'Archive',
    'ARCHIVE_MANY': 'Archive many',
    'SELECT_TO_ARCHIVE': 'Select to archive',
    'EDIT': 'Edit',
  	'DELETE': 'Delete',
    'DELETE_MANY': 'Delete many',
    'SELECT_TO_DELETE': 'Select to delete',

  	// Tab lists
  	'SHOPPING_LISTS': 'Shopping lists',
  	'LISTS': 'Lists',
  	'NO_LISTS_INFO': 'There are no lists currently.',
    'ADD_LIST': 'Add list',
    'TITLE': 'Title',
    'NOTES': 'Notes',
    'ADD_NOTES': 'Add notes',
    'DESCRIPTION': 'Description',

  	// Tab products
  	'PRODUCTS': 'Products',
    'NO_PRODUCTS_INFO': 'There are no products currently.',
    'PRICE': 'Price',
    'ADD_PRODUCT': 'Add product',
    'EDIT_PRODUCT': 'Edit product',

  	// Tab settings
  	'SETTINGS': 'Settings',
  	'LANGUAGE': 'Language',
  	'VERSION': 'Version',
  	'CHANGELOG': 'Changelog',

  });
}]);
