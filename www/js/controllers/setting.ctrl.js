angular.module('starter.controllers.setting', [])

.controller('SettingsCtrl', function($scope, $state, $translate, $ionicPopup, Settings, Lists, Sublists, Products) {
  /**
   * Main container for storing settings
   * @type {Object}
   */
  $scope.settings = Settings.get();

  /**
   * Used for changing navigation element's visibility
   * @type {Object}
   */
  $scope.show = {
  	changelog: function() {
  		alert('TODO: show changelog');
  	},
  	languagePicker: function() {
      $translate('LANGUAGE').then(function (language) {
        $scope.LanguageName = language;

        $scope.languagePickerPopup = $ionicPopup.show({
          templateUrl: 'templates/settings/language-picker-popover.html',
          title: $scope.LanguageName,
          scope: $scope,
          buttons: []
        });
      });
  	}
  }

  $scope.debug = {
    seedDB: function() {
      Lists.seed();
      Sublists.seed();
      Products.seed();
    }
  }

  /**
   * Used for changing values in settings
   * @type {Object}
   */
  $scope.change = {
    language: function(langKey) {
      $translate.use(langKey);
      $scope.settings.chosenLanguage = langKey;
      $scope.languagePickerPopup.close();
      Settings.set($scope.settings);
    }
  }
});
