angular.module('starter.controllers.list', [])

  .controller('ListsCtrl', function ($scope, $rootScope, $state, $ionicSideMenuDelegate, $ionicActionSheet, $ionicPopup, $ionicListDelegate, Lists) {
    /**
     * Main container for lists
     * @type {Array}
     */
    $rootScope.lists = Lists.allNormal();

    /**
     * Hack for hiding default back button
     * @type {Boolean}
     */
    $rootScope.hideBackButton = true;

    /**
     * Used for changing navigation element's visibility
     * @type {Object}
     */
    $scope.show = {
      searchPanel: false,
      sideMenu:    function () {
        return $ionicSideMenuDelegate.isOpenRight();
      },
      menu:        function () {
        $ionicSideMenuDelegate.toggleRight();
      }
    }

    /**
     * Used for changing navigation (quick element delete/archive, edit) with prompt
     * @type {Object}
     */
    $scope.try = {
      add:          function () {
        $state.go('tab.list-add');
        $scope.try.closeMenu();
      },
      archiveMany:  function () {
        $state.go('tab.list-archive');
        $scope.try.closeMenu();
      },
      deleteMany:   function () {
        $state.go('tab.list-delete');
        $scope.try.closeMenu();
      },
      edit:         function (item) {
        $state.go('tab.sublist', {listId: item.id})
        $ionicListDelegate.closeOptionButtons();
      },
      archive:      function (item) {
        $rootScope.itemTitle = item.title;
        $ionicPopup.confirm({
          title:    'Are you sure?',
          template: 'Are you sure you want to archive "{{itemTitle}}"?'
        }).then(function (res) {
          if (res) {
            Lists.archive(item);
            $rootScope.lists = Lists.allNormal();
            $ionicListDelegate.closeOptionButtons();
          }
        });
      },
      delete:       function (item) {
        $rootScope.itemTitle = item.title;
        $ionicPopup.confirm({
          title:    'Are you sure?',
          template: 'Are you sure you want to delete "{{itemTitle}}"?'
        }).then(function (res) {
          if (res) {
            Lists.delete(item);
            $rootScope.lists = Lists.allNormal();
            $ionicListDelegate.closeOptionButtons();
          }
        });
      },
      closeOptions: function () {
        $ionicListDelegate.closeOptionButtons();
      },
      closeMenu:    function () {
        if ($ionicSideMenuDelegate.isOpenRight())
          $ionicSideMenuDelegate.toggleRight();
        if ($ionicSideMenuDelegate.isOpenLeft())
          $ionicSideMenuDelegate.toggleLeft();
      }
    }
  })

  .controller('ListAddCtrl', function ($scope, $rootScope, $state, $ionicPopup, Lists) {
    /**
     * Main container for current item
     * @type {Model}
     */
    $scope.item = {};

    /**
     * Hack for hiding default back button
     * @type {Boolean}
     */
    $rootScope.hideBackButton = true;

    /**
     * Confirmation screen click action
     */
    $scope.confirmOperation = function () {
      if ($scope.item.title === undefined || $scope.item.title === null || $scope.item.title === '') {
        $ionicPopup.alert({
          title:    '<b class="bar-energized">Warning!</b>',
          template: '<span class="center">Title is required!</span>',
          cssClass: 'warning'
        });
        return;
      }

      Lists.create($scope.item.title, $scope.item.note);
      $state.go('tab.list');
    }

    /**
     * Cancel screen click action
     */
    $scope.cancelOperation = function () {
      $scope.item = null;
      $state.go('tab.list');
    }

    /**
     * Swipe navigation handler
     * @param  {string} direction Swipe direction event
     */
    $scope.swipe = function (direction) {
      if (direction == 'right')
        $scope.cancelOperation();
    }

    /**
     * On state change event listener
     * @param  {Object} event         Event object
     * @param  {State}  toState       The state being transitioned to
     * @param  {Object} toParams      The params supplied to the toState
     * @param  {State}  fromState     The current state, pre-transition.
     * @param  {Object} fromParams)   The params supplied to the fromState
     */
    $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (fromState.name == 'tab.list-add') {
        $rootScope.lists = Lists.allNormal();
      }
    })
  })

  .controller('ListViewerCtrl', function ($scope, $rootScope, $state, $ionicPopup, $stateParams, Lists) {
    /**
     * Main container for current item
     * @type {Model}
     */
    $scope.item = Lists.get($stateParams.listId);

    /**
     * Hack for hiding default back button
     * @type {Boolean}
     */
    $rootScope.hideBackButton = true;
    $scope.customBackButton = function () {
      $state.go('tab.list');
    };

    /**
     * State selected holder
     * @type {Object}
     */
    $scope.selected = {
      note: false
    };

    /**
     * Edit action handlers
     * @type {Object}
     */
    $scope.edit = {
      title: function () {
        $scope.titleNew = $scope.item.title;
        $ionicPopup.prompt({
          title:            'Change list title',
          inputType:        'text',
          inputPlaceholder: 'List title',
          defaultText:      $scope.titleNew
        }).then(function (res) {
          if (res !== undefined && res !== "") {
            $scope.item.title = res;
            $scope.update();
          }
        });
      },
      note:  {
        try:     function () {
          $scope.selected.note = true;
          $scope.noteOld = $scope.item.note;
        },
        confirm: function () {
          $scope.update();
        },
        cancel:  function () {
          $scope.item.note = $scope.noteOld;
          $scope.noteOld = null;
          $scope.selected.note = false;
        }
      }
    };

    /**
     * Update action with view reload
     */
    $scope.update = function () {
      $scope.selected.note = false;
      Lists.updateOrCreate($scope.item);
      $scope.item = Lists.get($stateParams.listId);
      $state.go($state.current, {listId: $scope.item.id}, {reload: true});
    };

    /**
     * Swipe navigation handler
     * @param  {string} direction Swipe direction event
     */
    $scope.swipe = function (direction) {
      if (direction == 'right')
        $scope.customBackButton();
      $state.go('tab.list');
    }

    /**
     * On state change event listener
     * @param  {Object} event         Event object
     * @param  {State}  toState       The state being transitioned to
     * @param  {Object} toParams      The params supplied to the toState
     * @param  {State}  fromState     The current state, pre-transition.
     * @param  {Object} fromParams)   The params supplied to the fromState
     */
    $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (fromState.name == 'tab.list-viewer') {
        $rootScope.lists = Lists.allNormal();
      }
    })
  })

  .controller('ListArchiveCtrl', function ($scope, $rootScope, $state, $ionicPopup, Lists) {
    /**
     * Main container for lists
     * @type {Array}
     */
    $rootScope.lists = Lists.allNormal();

    /**
     * Hack for hiding default back button
     * @type {Boolean}
     */
    $rootScope.hideBackButton = true;

    /**
     * Action executed when ng-click event on item
     * @param  {model} item [item on which event was fired]
     */
    $scope.itemClick = function (item) {
      item.toArchive = !item.toArchive;
    }

    /**
     * Confirmation screen click action
     */
    $scope.confirmOperation = function () {
      $rootScope.toArchiveCount = 0;
      angular.forEach($scope.lists, function (item) {
        if (item.toArchive)
          $rootScope.toArchiveCount++;
      });

      if ($rootScope.toArchiveCount == 0) {
        $ionicPopup.alert({
          title:    '<b class="bar-energized">Warning!</b>',
          template: '<span class="center">You haven\'t chosen any list!</span>',
          cssClass: 'warning'
        });
        return;
      }

      $ionicPopup.confirm({
        title:    'Are you sure?',
        template: 'Are you sure you want to archive {{toArchiveCount}} list(s)?'
      }).then(function (res) {
        if (res) {
          angular.forEach($scope.lists, function (item) {
            if (item.toArchive)
              Lists.archive(item);
          });
          $state.go('tab.list');
        }
      });
    }

    /**
     * Cancel screen click action
     */
    $scope.cancelOperation = function () {
      angular.forEach($scope.lists, function (item) {
        item.toArchive = false;
      })
      $state.go('tab.list');
    }

    /**
     * Swipe navigation handler
     * @param  {string} direction Swipe direction event
     */
    $scope.swipe = function (direction) {
      if (direction == 'right')
        $scope.cancelOperation();
    }

    /**
     * On state change event listener
     * @param  {Object} event         Event object
     * @param  {State}  toState       The state being transitioned to
     * @param  {Object} toParams      The params supplied to the toState
     * @param  {State}  fromState     The current state, pre-transition.
     * @param  {Object} fromParams)   The params supplied to the fromState
     */
    $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (fromState.name == 'tab.list-archive') {
        $rootScope.lists = Lists.allNormal();
      }
    })
  })

  .controller('ListDeleteCtrl', function ($scope, $rootScope, $state, $ionicPopup, Lists) {
    /**
     * Main container for lists
     * @type {Array}
     */
    $rootScope.lists = Lists.allNormal();

    /**
     * Hack for hiding default back button
     * @type {Boolean}
     */
    $rootScope.hideBackButton = true;

    /**
     * Action executed when ng-click event on item
     * @param  {model} item [item on which event was fired]
     */
    $scope.itemClick = function (item) {
      item.toDelete = !item.toDelete;
    }

    /**
     * Confirmation screen click action
     */
    $scope.confirmOperation = function () {
      $rootScope.toDeleteCount = 0;
      angular.forEach($scope.lists, function (item) {
        if (item.toDelete)
          $rootScope.toDeleteCount++;
      });

      if ($rootScope.toDeleteCount == 0) {
        $ionicPopup.alert({
          title:    '<b class="bar-energized">Warning!</b>',
          template: '<span class="center">You haven\'t chosen any list!</span>',
          cssClass: 'warning'
        });
        return;
      }

      $ionicPopup.confirm({
        title:    'Are you sure?',
        template: 'Are you sure you want to delete {{toDeleteCount}} list(s)?'
      }).then(function (res) {
        if (res) {
          angular.forEach($scope.lists, function (item) {
            if (item.toDelete)
              Lists.delete(item);
          })
          $state.go('tab.list');
        }
      });
    }

    /**
     * Cancel screen click action
     */
    $scope.cancelOperation = function () {
      angular.forEach($scope.lists, function (item) {
        item.toDelete = false;
      })
      $state.go('tab.list');
    }

    /**
     * Swipe navigation handler
     * @param  {string} direction Swipe direction event
     */
    $scope.swipe = function (direction) {
      if (direction == 'right')
        $scope.cancelOperation();
    }

    /**
     * On state change event listener
     * @param  {Object} event         Event object
     * @param  {State}  toState       The state being transitioned to
     * @param  {Object} toParams      The params supplied to the toState
     * @param  {State}  fromState     The current state, pre-transition.
     * @param  {Object} fromParams)   The params supplied to the fromState
     */
    $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (fromState.name == 'tab.list-delete') {
        $rootScope.lists = Lists.allNormal();
      }
    })
  })
