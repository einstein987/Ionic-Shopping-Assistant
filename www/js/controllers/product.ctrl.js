angular.module('starter.controllers.product', [])

  .controller('ProductsCtrl', function ($scope, $rootScope, $state, $ionicSideMenuDelegate, $ionicActionSheet, $ionicPopup, $ionicPopover, $ionicListDelegate, Products) {
  /**
   * Main container for products
   * @type {Array}
   */
  $rootScope.products = Products.allNormal();

  /**
   * Hack for hiding default back button
   * @type {Boolean}
   */
  $rootScope.hideBackButton = true;

  /**
   * Used for changing navigation element's visibility
   * @type {Object}
   */
  $scope.show = {
    searchPanel: false,
    sideMenu: function() {
      return $ionicSideMenuDelegate.isOpenRight();
    },
    menu: function(){
      $ionicSideMenuDelegate.toggleRight();
    },
    options: function(item) {
      alert("Asd");
    },
    image: function($event, item) {
      $scope.currentItem = item;

      $ionicPopover.fromTemplateUrl('templates/product/product-image-popover.html', {
        scope: $scope
      }).then(function(popover) {
        $scope.popover = popover;
        $scope.popover.show($event);
      });

      $scope.$on('popover.hidden', function() {
        $scope.popover = null;
        $scope.currentItem = null;
      });
    },
    itemOptions: function($event, item) {
      $scope.currentItem = item;

      $ionicPopover.fromTemplateUrl('templates/product/product-options-popover.html', {
        scope: $scope
      }).then(function(popover) {
        $scope.popover = popover;
        $scope.popover.show($event);
      });

      $scope.$on('popover.hidden', function() {
        $scope.popover = null;
        $scope.currentItem = null;
      });
    }
  }

  /**
   * Used for changing navigation (quick element delete/archive, edit) with prompt
   * @type {Object}
   */
  $scope.try = {
    add: function() {
      $state.go('tab.product-add');
    },
    archiveMany: function(){
      $state.go('tab.product-archive');
      $scope.try.closeMenu();
    },
    deleteMany: function() {
      $state.go('tab.product-delete');
      $scope.try.closeMenu();
    },
    edit: function(item) {
      $state.go('tab.product-editor', {productId: item.id})
      $ionicListDelegate.closeOptionButtons();
      $scope.popover.hide();
    },
    archive: function(item) {
      $scope.popover.hide();
      $rootScope.itemTitle = item.title;
      $ionicPopup.confirm({
        title: 'Are you sure?',
        template: 'Are you sure you want to archive "{{itemTitle}}"?'
      }).then(function(res) {
        if(res) {
          Products.archive(item);
          $rootScope.products = Products.allNormal();
          $ionicListDelegate.closeOptionButtons();
        }
      });
    },
    delete: function(item) {
      $scope.popover.hide();
      $rootScope.itemTitle = item.title;
      $ionicPopup.confirm({
        title: 'Are you sure?',
        template: 'Are you sure you want to delete "{{itemTitle}}"?'
      }).then(function(res) {
        if(res) {
          Products.delete(item);
          $rootScope.products = Products.allNormal();
          $ionicListDelegate.closeOptionButtons();
        }
      });
    },
    closeOptions: function() {
      $ionicListDelegate.closeOptionButtons();
    }
  }

  /**
   * Action executed when ng-click event on item
   * @param  {model} item [item on which event was fired]
   */
  $scope.itemClick = function(item){
    //$state.go('tab.product-editor', {listId: item.id});
    if($scope.lastSelectedItem)
    	$scope.lastSelectedItem.isSelected = false;
    if($scope.lastSelectedItem && item.id == $scope.lastSelectedItem.id){
      item.isSelected = false;
      $scope.lastSelectedItem = null;
    }
    else {
      item.isSelected = true;
      $scope.lastSelectedItem = item;
    }
    $ionicListDelegate.closeOptionButtons();
  }
})

.controller('ProductAddCtrl', function($scope, $rootScope, $ionicPopup, $state, $stateParams, Products) {
  $scope.product = Products.get($stateParams.productId);


  $rootScope.hideBackButton = true;
  $scope.customBackButton = function() {
    $state.go('tab.product');
  };


  $scope.selected = {
    note: false
  };

  $scope.selected = {
    price: false
  };

  $scope.itemClick = function(item){
    item.toArchive = !item.toArchive;
  }

  /**
   * Confirmation screen click action
   */
  $scope.confirmOperation = function() {
    if($scope.item.title === undefined || $scope.item.title === null || $scope.item.title === ''){
      $ionicPopup.alert({
        title: '<b class="bar-energized">Warning!</b>',
        template: '<span class="center">Title is required!</span>',
        cssClass: 'warning'
      });
      return;
    }

    Products.create($scope.item.title, $scope.item.note, $scope.item.price);
    $state.go('tab.list');
  }

  /**
   * Cancel screen click action
   */
  $scope.cancelOperation = function() {
    angular.forEach($scope.products, function(item){
      item.toArchive = false;
    })
    $state.go('tab.product');
  }

  /**
   * Swipe navigation handler
   * @param  {string} direction Swipe direction event
   */
  $scope.swipe = function (direction) {
    if(direction == 'right')
      $scope.cancelOperation();
  }

  $scope.edit = {
    title: function () {
      $scope.titleNew = $scope.item.title;
      $ionicPopup.prompt({
        title:            'Change Product title',
        inputType:        'text',
        inputPlaceholder: 'Product title',
        defaultText:      $scope.titleNew
      }).then(function (res) {
        if (res !== undefined && res !== "") {
          $scope.item.title = res;
          $scope.update();
        }
      });
    },
    note:  {
      try:     function () {
        $scope.selected.note = true;
        $scope.noteOld = $scope.item.note;
      },
      confirm: function () {
        $scope.update();
      },
      cancel:  function () {
        $scope.item.note = $scope.noteOld;
        $scope.noteOld = null;
        $scope.selected.note = false;
      },

      price: {
        try:     function () {
          $scope.selected.price = true;
          $scope.priceOld = $scope.item.price;
        },
        confirm: function () {
          $scope.update();
        },
        cancel:  function () {
          $scope.item.price = $scope.priceOld;
          $scope.priceOld = null;
          $scope.selected.price = false;
        }
      }
    }
  }

    /**
     * Update action with view reload
     */
    $scope.update = function() {
      $scope.selected.note = false;
      Products.updateOrCreate($scope.item);
      $scope.item = Products.get($stateParams.listId);
      $state.go($state.current, {productId: $scope.item.id}, {reload: true});
    };

    /**
     * Swipe navigation handler
     * @param  {string} direction Swipe direction event
     */
    $scope.swipe = function (direction) {
      if(direction == 'right')
        $scope.customBackButton();
      $state.go('tab.product');
    }

    /**
     * On state change event listener
     * @param  {Object} event         Event object
     * @param  {State}  toState       The state being transitioned to
     * @param  {Object} toParams      The params supplied to the toState
     * @param  {State}  fromState     The current state, pre-transition.
     * @param  {Object} fromParams)   The params supplied to the fromState
     */
    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      if(fromState.name == 'tab.product-viewer'){
        $rootScope.products = Products.allNormal();
    }
  })
})

  .controller('ProductArchiveCtrl', function($scope, $rootScope, $state, $ionicPopup, Lists){
    /**
     * Main container for lists
     * @type {Array}
     */
    $rootScope.products = Products.allNormal();

    /**
     * Hack for hiding default back button
     * @type {Boolean}
     */
    $rootScope.hideBackButton = true;

    /**
     * Action executed when ng-click event on item
     * @param  {model} item [item on which event was fired]
     */
    $scope.itemClick = function(item){
      item.toArchive = !item.toArchive;
    }

    /**
     * Confirmation screen click action
     */
    $scope.confirmOperation = function() {
      $rootScope.toArchiveCount = 0;
      angular.forEach($scope.products, function(item){
        if(item.toArchive)
          $rootScope.toArchiveCount++;
      });

      if($rootScope.toArchiveCount == 0){
        $ionicPopup.alert({
          title: '<b class="bar-energized">Warning!</b>',
          template: '<span class="center">You haven\'t chosen any product!</span>',
          cssClass: 'warning'
        });
        return;
      }

      $ionicPopup.confirm({
        title: 'Are you sure?',
        template: 'Are you sure you want to archive {{toArchiveCount}} product(s)?'
      }).then(function(res) {
        if(res) {
          angular.forEach($scope.products, function(item){
            if(item.toArchive)
              Products.archive(item);
          });
          $state.go('tab.product');
        }
      });
    }

    /**
     * Cancel screen click action
     */
    $scope.cancelOperation = function() {
      angular.forEach($scope.products, function(item){
        item.toArchive = false;
      })
      $state.go('tab.product');
    }

    /**
     * Swipe navigation handler
     * @param  {string} direction Swipe direction event
     */
    $scope.swipe = function (direction) {
      if(direction == 'right')
        $scope.cancelOperation();
    }

    /**
     * On state change event listener
     * @param  {Object} event         Event object
     * @param  {State}  toState       The state being transitioned to
     * @param  {Object} toParams      The params supplied to the toState
     * @param  {State}  fromState     The current state, pre-transition.
     * @param  {Object} fromParams)   The params supplied to the fromState
     */
    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      if(fromState.name == 'tab.product-archive'){
        $rootScope.products = Products.allNormal();
      }
    })
  })

  .controller('ProductEditorCtrl', function($scope, $rootScope, $state, $stateParams, $ionicPopup, Products) {
    $scope.item = Products.get($stateParams.productId);
    $scope.item.paramsConverted = "";

    angular.forEach($scope.item.params, function(value, key){
      $scope.item.paramsConverted += value + "\n";
    });

    $scope.confirmOperation = function() {
      if ($scope.item.title === undefined || $scope.item.title === null || $scope.item.title === '') {
        $ionicPopup.alert({
          title:    '<b class="bar-energized">Warning!</b>',
          template: '<span class="center">Title is required!</span>',
          cssClass: 'warning'
        });
        return;
      }

      $scope.item.params = $scope.item.paramsConverted.split("\n");
      Products.updateOrCreate($scope.item);
      $state.go('tab.product');
    }

    $scope.cancelOperation = function() {
      angular.forEach($scope.products, function(item){
        item.toDelete = false;
      })
      $state.go('tab.product');
    }
    
    /**
     * Swipe navigation handler
     * @param  {string} direction Swipe direction event
     */
    $scope.swipe = function (direction) {
      if(direction == 'right')
        $scope.cancelOperation();
    }

    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      if(fromState.name == 'tab.product-editor'){
        $rootScope.products = Products.allNormal();
      }
    })
  })

  .controller('ProductDeleteCtrl', function($scope, $rootScope, $state, $ionicPopup, Products){
    /**
     * Main container for lists
     * @type {Array}
     */
    $rootScope.products = Products.allNormal();

    /**
     * Hack for hiding default back button
     * @type {Boolean}
     */
    $rootScope.hideBackButton = true;

    /**
     * Action executed when ng-click event on item
     * @param  {model} item [item on which event was fired]
     */
    $scope.itemClick = function(item){
      item.toDelete = !item.toDelete;
    }

    /**
     * Confirmation screen click action
     */
    $scope.confirmOperation = function() {
      $rootScope.toDeleteCount = 0;
      angular.forEach($scope.products, function(item){
        if(item.toDelete)
          $rootScope.toDeleteCount++;
      });

      if($rootScope.toDeleteCount == 0){
        $ionicPopup.alert({
          title: '<b class="bar-energized">Warning!</b>',
          template: '<span class="center">You haven\'t chosen any product!</span>',
          cssClass: 'warning'
        });
        return;
      }

      $ionicPopup.confirm({
        title: 'Are you sure?',
        template: 'Are you sure you want to delete {{toDeleteCount}} product(s)?'
      }).then(function(res) {
        if(res) {
          angular.forEach($scope.products, function(item){
            if(item.toDelete)
              Lists.delete(item);
          })
          $state.go('tab.product');
        }
      });
    }

    /**
     * Cancel screen click action
     */
    $scope.cancelOperation = function() {
      angular.forEach($scope.products, function(item){
        item.toDelete = false;
      })
      $state.go('tab.product');
    }

    /**
     * Swipe navigation handler
     * @param  {string} direction Swipe direction event
     */
    $scope.swipe = function (direction) {
      if(direction == 'right')
        $scope.cancelOperation();
    }

    /**
     * On state change event listener
     * @param  {Object} event         Event object
     * @param  {State}  toState       The state being transitioned to
     * @param  {Object} toParams      The params supplied to the toState
     * @param  {State}  fromState     The current state, pre-transition.
     * @param  {Object} fromParams)   The params supplied to the fromState
     */
    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      if(fromState.name == 'tab.product-delete'){
        $rootScope.products = Products.allNormal();
      }
    })
  })
