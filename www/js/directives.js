angular.module('starter.directives', [])

.directive('searchPanel', function() {
  return {
    transclude: true,
    scope: { 
    	show: '=actuator',
    	filter: '=filter',
    	display: '=display'
    },
    templateUrl: 'templates/search-panel.html'
  };
});