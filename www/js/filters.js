angular.module('starter.filters', ['starter.filters.list'])

.filter('dateFromNow', function() {
  return function(input) {
  	if(input == '') 
  		return null;

  	var now = new Date();
    var inputDate = new Date();
    inputDate.setTime(input);

    if(now.getFullYear() - inputDate.getFullYear() > 0)
    	return inputDate.getFullYear();
    else if(now.getMonth() - inputDate.getMonth() > 0 || now.getDay() - inputDate.getDay() > 7){
    	var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    	return '' + inputDate.getDay() + ' ' + monthShortNames[inputDate.getMonth()];
    }
    else if(Math.abs(inputDate.getDay() - now.getDay()) > 0)
    	return '' + Math.abs(inputDate.getDay() - now.getDay()) + 'd';
    else if(Math.abs(inputDate.getHours() - now.getHours()) > 0)
    	return '' + Math.abs(inputDate.getHours() - now.getHours()) + 'h';
    else if(Math.abs(inputDate.getMinutes() - now.getMinutes()) > 0)
    	return '' + Math.abs(inputDate.getMinutes() - now.getMinutes()) + 'min';
    else if(!isNaN(inputDate.getSeconds() - now.getSeconds()))
    	return '' + Math.abs(inputDate.getSeconds() - now.getSeconds()) + 's';
    else
      return;
  };
});