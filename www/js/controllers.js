angular.module('starter.controllers', [
  'starter.controllers.list',
  'starter.controllers.sublist',
  'starter.controllers.product',
  'starter.controllers.setting'
])