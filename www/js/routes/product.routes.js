angular.module('starter.router.product', ['ionic', 'starter.router.tab'])
.config(function($stateProvider, $urlRouterProvider) {

	$stateProvider

	.state('tab.product', {
    url: '/product',
    views: {
      'tab-product': {
        templateUrl: 'templates/product/product-list.html',
        controller: 'ProductsCtrl'
      }
    }
  })

  .state('tab.product-add', {
  	url:'/product/add',
  	views: {
  		'tab-product': {
  			templateUrl: 'templates/product/product-add.html',
  			controller: 'ProductAddCtrl'
  		}
  	}
  })

  .state('tab.product-archive', {
  	url:'/product/archive',
  	views: {
  		'tab-product': {
  			templateUrl: 'templates/product/product-archive.html',
  			controller: 'ProductArchiveCtrl'
  		}
  	}
  })

  .state('tab.product-delete', {
  	url:'/product/delete',
  	views: {
  		'tab-product': {
  			templateUrl: 'templates/product/product-delete.html',
  			controller: 'ProductDeleteCtrl'
  		}
  	}
  })

  .state('tab.product-editor', {
    url:'/product/edit/:productId',
    views: {
      'tab-product': {
        templateUrl: 'templates/product/product-editor.html',
        controller: 'ProductEditorCtrl'
      }
    }
  })
});