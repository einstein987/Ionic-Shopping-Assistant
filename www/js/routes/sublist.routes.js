angular.module('starter.router.sublist', ['ionic', 'starter.router.tab'])
.config(function($stateProvider, $urlRouterProvider) {

	$stateProvider

	.state('tab.sublist', {
    url:   '/sublist/:listId',
    views: {
      'tab-sublist': {
        templateUrl: 'templates/sublist/sublist.html',
        controller: 'SublistsCtrl'
      }
    }
  })

  .state('tab.sublist-add', {
  	url:'/sublist/add',
  	views: {
  		'tab-sublist': {
  			templateUrl: 'templates/sublist/sublist-add.html',
  			controller: 'SublistAddCtrl'
  		}
  	}
  })

  .state('tab.sublist-archive', {
  	url:'/sublist/archive',
  	views: {
  		'tab-sublist': {
  			templateUrl: 'templates/sublist/sublist-archive.html',
  			controller: 'SublistArchiveCtrl'
  		}
  	}
  })

  .state('tab.sublist-delete', {
  	url:'/sublist/delete',
  	views: {
  		'tab-sublist': {
  			templateUrl: 'templates/sublist/sublist-delete.html',
  			controller: 'SublistDeleteCtrl'
  		}
  	}
  })

  .state('tab.sublist-viewer', {
    url:'/sublist/view/:sublistId',
    views: {
      'tab-sublist': {
        templateUrl: 'templates/sublist/sublist-viewer.html',
        controller: 'SublistViewerCtrl'
      }
    }
  })
});
