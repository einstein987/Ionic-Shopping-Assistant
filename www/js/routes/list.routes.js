angular.module('starter.router.list', ['ionic', 'starter.router.tab'])
.config(function($stateProvider, $urlRouterProvider) {

	$stateProvider

	.state('tab.list', {
    url: '/list',
    views: {
      'tab-list': {
        templateUrl: 'templates/list/list.html',
        controller: 'ListsCtrl'
      }
    }
  })

  .state('tab.list-add', {
  	url:'/list/add',
  	views: {
  		'tab-list': {
  			templateUrl: 'templates/list/list-add.html',
  			controller: 'ListAddCtrl'
  		}
  	}
  })

  .state('tab.list-archive', {
  	url:'/list/archive',
  	views: {
  		'tab-list': {
  			templateUrl: 'templates/list/list-archive.html',
  			controller: 'ListArchiveCtrl'
  		}
  	}
  })

  .state('tab.list-delete', {
  	url:'/list/delete',
  	views: {
  		'tab-list': {
  			templateUrl: 'templates/list/list-delete.html',
  			controller: 'ListDeleteCtrl'
  		}
  	}
  })

  .state('tab.list-viewer', {
    url:'/list/view/:listId',
    views: {
      'tab-list': {
        templateUrl: 'templates/list/list-viewer.html',
        controller: 'ListViewerCtrl'
      }
    }
  })
});