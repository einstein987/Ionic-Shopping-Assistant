angular.module('starter.router', ['ionic', 
  'starter.router.list',
  'starter.router.sublist',
  'starter.router.product'
])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-settings.html',
        controller: 'SettingsCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/list');

});