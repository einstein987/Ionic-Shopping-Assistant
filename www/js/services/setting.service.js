angular.module('starter.services.setting', ['starter.factories.data'])

.factory('Settings', function($database) {

	var settings = {
		appVersion: 'v0.1',
		build: null,
    chosenLanguage: 'en'
	}

	if(window.cordova){
		cordova.getAppVersion.getVersionNumber().then(function (version) {
			if(version != null)
	  		settings.appVersion = version;
	  });

		cordova.getAppVersion.getVersionCode().then(function (buildCode) {
	  	settings.build = buildCode;
	  });
	}

	var tableName = 'settings';

	return {
		get: function() {
			var data = $database.getObject(tableName);
			if(data !== null)
				settings = data;
			return settings;
		},
		set: function(model) {
			settings = model;
			$database.setObject(tableName, model);
		}
	}
})