angular.module('starter.services.list', ['starter.factories.data'])

  .factory('Lists', function ($database) {

    var tableName = 'list';
    var list = [];

    var seed = [{
      id:           0,
      title:        'Essentials',
      creationDate: '1453813091156',
      updateDate:   '1453814091256',
      isDeleted:    false,
      isArchived:   false
    }, {
      id:           1,
      title:        'Tools',
      creationDate: '1455344091256',
      updateDate:   '1455934091256',
      isDeleted:    false,
      isArchived:   false
    }, {
      id:           3,
      title:        'Another list',
      creationDate: '1455344091256',
      updateDate:   '1455934091256',
      isDeleted:    false,
      isArchived:   false
    }, {
      id:           4,
      title:        'Deleted list',
      creationDate: '1455344091256',
      updateDate:   '1455934091256',
      isDeleted:    true,
      isArchived:   false
    }, {
      id:           5,
      title:        'Archived list',
      creationDate: '1455344091256',
      updateDate:   '1455934091256',
      isDeleted:    false,
      isArchived:   true
    }, {
      id:           6,
      title:        'Note test',
      note:         'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.',
      creationDate: '1455897165273',
      updateDate:   '1455897165273',
      isDeleted:    false,
      isArchived:   false
    }];

    var lastId = 6;

    return {
      all:            function () {
        list = $database.getObject(tableName);
        return list;
      },
      seed:           function () {
        $database.setObject(tableName, seed);
      },
      allArchived:    function () {
        list = $database.getObject(tableName);
        var output = [];
        for (var i = 0; i < list.length; i++) {
          if (list[i].isArchived)
            output.push(list[i]);
        }
        return output;
      },
      allDeleted:     function () {
        list = $database.getObject(tableName);
        var output = [];
        for (var i = 0; i < list.length; i++) {
          if (list[i].isDeleted)
            output.push(list[i]);
        }
        return output;
      },
      allNormal:      function () {
        list = $database.getObject(tableName);
        var output = [];
        for (var i = 0; i < list.length; i++) {
          if (!list[i].isDeleted && !list[i].isArchived)
            output.push(list[i]);
        }
        return output;
      },
      archive:        function (item) {
        item.toArchive = false;
        item.isArchived = true;
        $database.setObject(tableName, list);
      },
      delete:         function (item) {
        item.toDelete = false;
        item.isDeleted = true;
        $database.setObject(tableName, list);
      },
      get:            function (itemId) {
        list = $database.getObject(tableName);
        for (var i = 0; i < list.length; i++) {
          if (list[i].id === parseInt(itemId)) {
            return list[i];
          }
        }
        return null;
      },
      updateOrCreate: function (model) {
        var newElement = true;
        var now = new Date();

        if (model.id !== null && model.id !== undefined) {
          for (var i = 0; i < list.length; i++) {
            if (list[i].id == model.id) {
              newElement = false;
              list[i].title = model.title;
              list[i].note = model.note;
              list[i].isDeleted = model.isDeleted;
              list[i].isArchived = model.isArchived;
              list[i].updateDate = now.getTime();
            }
          }
        }
        if (newElement || model.id === null || model.id === undefined) {
          list.unshift({
            id:           (lastId + 1),
            title:        model.title,
            note:         model.note,
            creationDate: now.getTime(),
            updateDate:   now.getTime(),
            isDeleted:    false,
            isArchived:   false
          });
          lastId++;
        }
        $database.setObject(tableName, list);
      },
      create:         function (titleText, noteText) {
        var now = new Date();
        list.unshift({
          id:           (lastId + 1),
          title:        titleText,
          note:         noteText,
          creationDate: now.getTime(),
          updateDate:   now.getTime(),
          isDeleted:    false,
          isArchived:   false
        });
        lastId++;
        $database.setObject(tableName, list);
      }
    };
  })
