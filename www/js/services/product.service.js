angular.module('starter.services.product', ['starter.factories.data'])

  .factory('Products', function ($database) {
    var tableName = "products";

    var products = []

    var seed = [{
      id:          0,
      title:       'Samsung WF70F5E0W2W',
      description: '1 137,98 zł',
      params: ['Typ załadunku: od przodu', 
               'Wymiary (GxSxW): 55 x 60 x 85 cm', 
               'Maksymalne obroty wirowania: 1200 obr/min', 
               'Wielkość załadunku: 7 kg'],
      photo:       'img/samsung1.jpg'
    }, {
      id:          1,
      title:       'Samsung GE83X',
      description: '297 zł',
      params: ['Pojemność: 23 litry', 
               'Wymiary (GxSxW): 37,4 x 48,9 x 27,5 cm',
               'Opcje podstawowe: gotowanie, grill, podgrzewanie, rozmrażanie', 
               'Moc mikrofal: 800 W',
               'Moc grilla: 1100 W'],
      photo:       'img/samsung2.jpg'
    }, {
      id:          2,
      title:       'NavRoad VIVO S6',
      description: '519 zł',
      params: ['Wyświetlacz: 6 cali, 800 x 480 px', 'Wgrane mapy: brak'],
      photo:       'img/navroad.jpg'
    }, {
      id:          3,
      title:       'Makita UC3541A',
      description: '421 zł',
      params: ['Typ silnika: elektryczny',
               'Moc: 1,8 kW',
               'Długość prowadnicy: 35 cm',
               'Hamulec bezwładnościowy: tak'],
      photo:       'img/makita.jpg'
    }];

    var lastId = 3;

    return {
      all:            function () {
        products = $database.getObject(tableName);
        return products;
      },
      seed:           function () {
        $database.setObject(tableName, seed);
      },
      allArchived:    function () {
        products = $database.getObject(tableName);
        var output = [];
        for (var i = 0; i < products.length; i++) {
          if (products[i].isArchived)
            output.push(products[i]);
        }
        return output;
      },
      allDeleted:     function () {
        products = $database.getObject(tableName);
        var output = [];
        for (var i = 0; i < products.length; i++) {
          if (products[i].isDeleted)
            output.push(products[i]);
        }
        return output;
      },
      allNormal:      function () {
        products = $database.getObject(tableName);
        var output = [];
        for (var i = 0; i < products.length; i++) {
          if (!products[i].isDeleted && !products[i].isArchived)
            output.push(products[i]);
        }
        return output;
      },
      archive:        function (item) {
        item.toArchive = false;
        item.isArchived = true;
        $database.setObject(tableName, products);
      },
      delete:         function (item) {
        item.toDelete = false;
        item.isDeleted = true;
        $database.setObject(tableName, products);
      },
      get:            function (productId) {
        products = $database.getObject(tableName);
        for (var i = 0; i < products.length; i++) {
          if (products[i].id === parseInt(productId)) {
            return products[i];
          }
        }
        return null;
      },
      updateOrCreate: function (model) {
        var newElement = true;
        var now = new Date();

        if (model.id !== null && model.id !== undefined) {
          for (var i = 0; i < products.length; i++) {
            if (products[i].id == model.id) {
              newElement = false;
              products[i].title = model.title;
              products[i].description = model.description;
              products[i].isDeleted = model.isDeleted;
              products[i].isArchived = model.isArchived;
              products[i].updateDate = now.getTime();
            }
          }
        }
        if (newElement || model.id === null || model.id === undefined) {
          products.unshift({
            id:           (lastId + 1),
            title:        model.title,
            description:  model.description,
            creationDate: now.getTime(),
            updateDate:   now.getTime(),
            isDeleted:    false,
            isArchived:   false
          });
          lastId++;
        }
        $database.setObject(tableName, products);
      },
      create:         function (titleText, descriptionText) {
        var now = new Date();
        products.unshift({
          id:           (lastId + 1),
          title:        titleText,
          description:  descriptionText,
          creationDate: now.getTime(),
          updateDate:   now.getTime(),
          isDeleted:    false,
          isArchived:   false
        });
        lastId++;
        $database.setObject(tableName, products);
      }
    };
  });
