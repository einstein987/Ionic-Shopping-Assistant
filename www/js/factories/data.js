var db = 'localstorage';

angular.module('starter.factories.data', ['ngCordova'])

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}])

.factory('$sqliteInterface', ['$window', function($window, $cordovaSQLite) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}])

.factory('$database', ['$window', '$localstorage', '$sqliteInterface', function($window, $localstorage, $sqliteInterface) {
  return {
    set: function(key, value) {
      if(db == "sqlite")
        $sqliteInterface.set(key, value);
      else if(db == "localstorage")
        $localstorage.set(key, value);
    },
    get: function(key, defaultValue) {
      if(db == "sqlite")
        return $sqliteInterface.get(key, defaultValue);
      else if(db == "localstorage")
        return $localstorage.get(key, defaultValue);
    },
    setObject: function(key, value) {
      if(db == "sqlite")
        return $sqliteInterface.setObject(key, value);
      else if(db == "localstorage")
        return $localstorage.setObject(key, value);
    },
    getObject: function(key) {
      if(db == "sqlite")
        return $sqliteInterface.getObject(key);
      else if(db == "localstorage")
        return $localstorage.getObject(key);
    }
  }
}]);