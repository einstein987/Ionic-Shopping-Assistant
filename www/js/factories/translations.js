angular.module('starter.factories.translations', [
	'pascalprecht.translate', 
	'translation.en',
	'translation.pl'
])

.config(['$translateProvider', function ($translateProvider) {

  $translateProvider.preferredLanguage('pl');
  $translateProvider.usePostCompiling(true);
  $translateProvider.useSanitizeValueStrategy('sanitize');

}]);